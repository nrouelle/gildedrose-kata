﻿using NUnit.Framework;
using System.Collections.Generic;
using NUnit.Framework.Internal;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        [Test]
        public void ConjuredQualityShouldDegradeTwiceAsFastAsNormalItem()
        {
            IList<Item> Items = new List<Item> { new ConjuredQualityItem { Name = "Conjured Mana Cake", SellIn = 12, Quality = 20 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(18, Items[0].Quality);
        }
    }


    public class UpdateItemShould
    {
        [TestFixture]
        public class WithStandardItem
        {
            [Test]
            public void DecreaseSellInValue()
            {
                IList<Item> Items = new List<Item> {new StandardItem {Name = "+5 Dexterity Vest", SellIn = 20, Quality = 19}};
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(19, Items[0].SellIn);
            }

            [Test]
            public void DecreaseQualityValue()
            {
                IList<Item> Items = new List<Item> {new StandardItem { Name = "+5 Dexterity Vest", SellIn = 20, Quality = 19}};
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(18, Items[0].Quality);
            }

            [Test]
            public void QualityDegradesTwiceIfSellByDateHasPassed()
            {
                IList<Item> Items = new List<Item> {new StandardItem { Name = "foo", SellIn = -1, Quality = 19}};
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(17, Items[0].Quality);
            }

            [Test]
            public void QualityCouldNotBeNegative()
            {
                IList<Item> Items = new List<Item> {new StandardItem { Name = "foo", SellIn = -1, Quality = 1}};
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.IsTrue(Items[0].Quality >= 0);
            }

        }

        [TestFixture]
        public class WithLegendaryItem
        {
            [Test]
            public void SellInNeverDecrease()
            {
                IList<Item> Items = new List<Item>
                {
                    new LegendaryItem {Name = "Sulfuras, Hand of Ragnaros", SellIn = 12, Quality = 20}
                };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(12, Items[0].SellIn);
            }

            [Test]
            public void QualityNeverDecrease()
            {
                IList<Item> Items = new List<Item>
                {
                    new LegendaryItem {Name = "Sulfuras, Hand of Ragnaros", SellIn = 12, Quality = 20}
                };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(20, Items[0].Quality);
            }
        }

        [TestFixture]
        public class WithQualityAgedItem
        {
            [Test]
            public void QualityIncreaseEachDay()
            {
                IList<Item> Items = new List<Item> { new QualityAgedItem { Name = "Aged Brie", SellIn = 12, Quality = 8 } };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(9, Items[0].Quality);
            }

            [Test]
            public void SellInStillDecreaseEachDay()
            {
                IList<Item> Items = new List<Item> { new QualityAgedItem { Name = "Aged Brie", SellIn = 12, Quality = 8 } };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(11, Items[0].SellIn);
            }

            [Test]
            public void QualityCannotBeGreaterThan50()
            {
                IList<Item> Items = new List<Item> { new QualityAgedItem { Name = "Aged Brie", SellIn = 12, Quality = 50 } };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(50, Items[0].Quality);
            }
        }

        [TestFixture]
        public class WithEventItem
        {
            [Test]
            [TestCase(10)]
            [TestCase(9)]
            [TestCase(8)]
            [TestCase(7)]
            [TestCase(6)]
            public void QualityShouldIncreaseBy2TenDaysOrLessBeforeSellInDate(int sellIn)
            {
                IList<Item> Items = new List<Item> { new EventItem { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = sellIn, Quality = 10 } };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(12, Items[0].Quality);
            }

            [Test]
            [TestCase(5)]
            [TestCase(4)]
            [TestCase(3)]
            [TestCase(2)]
            [TestCase(1)]
            public void QualityShouldIncreaseBy3FiveDaysOrLessBeforeSellInDate(int sellIn)
            {
                IList<Item> Items = new List<Item> { new EventItem { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = sellIn, Quality = 10 } };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(13, Items[0].Quality);
            }

            [Test]
            public void QualityShouldBeNullWhenSellInDateHasPassed()
            {
                IList<Item> Items = new List<Item> { new EventItem { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 14 } };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(0, Items[0].Quality);
            }

            [Test]
            public void SellDataShouldDecrementNormally()
            {
                IList<Item> Items = new List<Item> { new EventItem { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 11, Quality = 14 } };
                GildedRose app = new GildedRose(Items);
                app.UpdateQuality();
                Assert.AreEqual(10, Items[0].SellIn);
            }
        }
    }
}
